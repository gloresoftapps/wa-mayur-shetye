# Weather App

Weather App is responsible for:
- Showing current weather data for a given city
- Showing user search history

### Dependencies

Weather App requires the below services to be running: 
* HSQL database, 
* Weather App backend, 
* Weather App UI

### System requirements

To run the application locally below utilities must be installed.
- Java v1.8
- nginx server v1.16.1 (Included as part of artifacts)

### Deployment Instructions

All artifacts are present under 'artifacts' folder.

1. Execute 'database\start.bat' file. 
   This will start the HSQL database on port 9001.
   The script will create a database named 'weatherdb'
2. Open command prompt and navigate to 'artifacts\weather-app-backend' directory. 
   - Run 'java -jar weather-app-backend.jar'
     This will start the backend microservice on port 9090
3. Navigate to 'artifacts\nginx' directory. 
   - Execute nginx.exe


### Usage
   
After performing the above steps, as per default configuration the application can be accessed at http://localhost:8080/
Register an admin user with email as 'admin@admin' and password 'admin' 
Register normal user with any username and password

### Known Issues/Improvements

1. UX and overall styling to be improved
2. Role information is currently passed as a seperate field in response body along with Bearer token. 
   Thus, it is not encoded and can be seen as plain text. 
   This information can be passed in the token itself thus improving security.
3. 'Sunrise' and 'Sunset' is always returned as null by the open weather API. 
   Hence, as of now dummy random values are being populated in these fields.
4. Update feature for 'Sunrise' and 'Sunset' shows the date in ISO date format. A simpler user friendly date format can be used to improve user experience.
5. Email and Date of Birth validations to be improved
6. JUnit test cases are currently written only for Service layer. The test coverage is to be increased to include web and repository layer tests.   


   
